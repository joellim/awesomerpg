using System;
using System.Diagnostics.CodeAnalysis;
using HumbleProgrammer.Characters;

namespace HumbleProgrammer
{
    [ExcludeFromCodeCoverage]
    public class Player
    {
        public Guid Id { get; }
        public Character[] Characters { get; }

        public Player(Guid id, Character[] characters)
        {
            Id = id;
            Characters = characters;
        }
    }
}