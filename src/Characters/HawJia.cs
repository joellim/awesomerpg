using System;
using System.Linq;

namespace HumbleProgrammer.Characters
{
    public class HawJia : Character
    {
        public delegate int Calculator(int numWins);

        public static Calculator LevelCalculator { get; } = (wins) => (int) Math.Pow(2, wins);
        public static Calculator CooldownCalculator { get; } = (level) => 5 - level;
        public static Calculator DamageCalculator { get; } = (level) => (int) Math.Pow(level, 2);
        public static Calculator SpeedCalculator { get; } = (level) => (int) Math.Pow(level, 2);

        private int Wins { get; }
        
        public int Level { get; }

        public string Name { get; }

        public int Cooldown
        {
            get { return CooldownCalculator(Level); }
        }

        public int Damage
        {
            get { return DamageCalculator(Level); }
        }

        public int Hp { get; set; } = 10;

        public int Speed
        {
            get { return SpeedCalculator(Level); }
        }

        public HawJia(int numWins, string name = "HawJia")
        {
            if (numWins < 0)
            {
                throw new ArgumentException();
            }

            Wins = numWins;
            Level = LevelCalculator(numWins);
            Name = name;
        }

        public string[] SpecialAction(Character[] allies, Character[] enemies)
        {
            var rnd = new Random();
            var allyIndex = rnd.Next(allies.Length);
            var enemyIndex = rnd.Next(enemies.Length);

            var enemy = enemies[enemyIndex];
            var ally = allies[allyIndex];
            enemies[enemyIndex] = ally;
            allies[allyIndex] = enemy;
            
            return new[] {$"{ally.Name} and {enemy.Name} swapped teams!"};
        }
    }
}