using System;

namespace HumbleProgrammer.Characters
{
    public class CustomChar : Character
    {
        private int Level;

        public CustomChar(int Wins, string name)
        {
            if (Wins < 0)
            {
                throw new System.ArgumentException("Wins cannot be negative");
            }

            Name = name;
            Level = WinsToLevel(Wins);
            Hp = Level + 5;
        }

        int WinsToLevel(int Wins)
        {
            return Wins;
        }

        public string Name { get; }

        public int Cooldown => Math.Min(2, 5 - Level); 

        public int Damage => 1 + Level;


        public int Hp { get; set; }

        public int Speed => 1 + Level;


        public string[] SpecialAction(Character[] allies, Character[] enemies) {
            int damage = Damage;
            string[] logs = new string[enemies.Length + 1];
            logs[0] = Name + " casted Hadouken!";
            int i = 1;
            foreach (Character enemy in enemies) {
                enemy.Hp -= damage;
                damage += 1;
                logs[i] = enemy.Name + " took " + damage + " damage.";
                i += 1;
            }

            return logs;
        }
    }
}