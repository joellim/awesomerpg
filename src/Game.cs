using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using HumbleProgrammer.Characters;

namespace HumbleProgrammer
{
    [ExcludeFromCodeCoverage]
    public class Game
    {
        private readonly Player _player1;
        private readonly Player _player2;

        public Game(Player player1, Player player2)
        {
            _player1 = player1;
            _player2 = player2;
        }

        public enum Winner
        {
            Player1,
            Player2,
            None
        };

        // Check if there is a winner, or not
        public Winner CheckWinner(Character[] player1Units, Character[] player2Units)
        {
            var player1UnitsTotalHealth = player1Units.Select(x => x.Hp).Sum();
            var player2UnitsTotalHealth = player2Units.Select(x => x.Hp).Sum();

            if (player2UnitsTotalHealth == 0)
            {
                return Winner.Player1;
            }

            return player1UnitsTotalHealth == 0 ? Winner.Player2 : Winner.None;
        }

        // Should run through a whole battle, return the logs of the battle and return the winning player!
        public (string[], Player) SimulateMatch()
        {
            var team1 = _player1.Characters;
            var team2 = _player2.Characters;
            var winner = Winner.None;

            IEnumerable<string> logs = new List<string>();
            var step = 0;
            
            while (winner == Winner.None)
            {
                logs = logs.Append("-----------------------------------------------");
                logs = logs.Append($"Start of step {step}\n");
                logs = logs.Append("Actions:");
                logs = logs.Concat(Step(step, team1, team2));
                winner = CheckWinner(team1, team2);
                
                logs = logs.Append("\n" + getStatus(team1, team2));
                logs = logs.Append($"End of step {step}");
                logs = logs.Append("-----------------------------------------------\n");

                step++;
            }

            var playerWinner = winner == Winner.Player1 ? _player1 : _player2;

            Console.WriteLine(logs.ToArray().Length);
            
            return (logs.ToArray(), playerWinner);
        }

        private string getStatus(Character[] team1, Character[] team2)
        {
            var result = "";
            result += "Player 1 status:\n";
            team1.ToList().ForEach(x => result += $"{x.Name} : {x.Hp}\n");
            result += "\nPlayer 2 status:\n";
            team2.ToList().ForEach(x => result += $"{x.Name} : {x.Hp}\n");
            return result;
        }
        

        // Based on the number of steps ran, it should decide to cast skill or do normal attack, and return the logs
        // for number of steps taken
        public IEnumerable<string> Step(int step, Character[] player1Units, Character[] player2Units)
        {
            IEnumerable<string> logs = new List<string>();

            var teams = new Character[][]{player1Units, player2Units};
            var fasterTeamIndex = GetFasterTeam(player1Units, player2Units) - 1;
            var currentTeam = teams[fasterTeamIndex];
            var otherTeam = teams[(fasterTeamIndex + 1) % 2];

            var minLength = Math.Min(player1Units.Length, player2Units.Length);
            
            for (var turn = 0; turn < minLength; turn++)
            {
                TurnAction(step, turn, currentTeam, otherTeam, ref logs);
                TurnAction(step, turn, otherTeam, currentTeam, ref logs);
            }

            for (var turn = minLength; turn < currentTeam.Length; turn++)
            {
                TurnAction(step, turn, currentTeam, otherTeam, ref logs);
            }
            
            for (var turn = minLength; turn < otherTeam.Length; turn++)
            {
                TurnAction(step, turn, otherTeam, currentTeam, ref logs);
            }
            
            return logs;
        }

        private void TurnAction(int step, int turn, Character[] attackingTeam, Character[] victimTeam, ref IEnumerable<string> logs)
        {
            var currentChar = attackingTeam[turn];

            if (currentChar.Hp == 0)
            {
                return;
            }
            
            if (ShouldCastSpell(step, currentChar))
            {
                logs = logs.Concat(currentChar.SpecialAction(attackingTeam, victimTeam));
            }
            else
            {
                var victim = getFirstLivingCharacter(victimTeam);

                if (victim != null)
                {
                    logs = logs.Append(Attack(currentChar, victim));
                }
            }
        }

        private Character getFirstLivingCharacter(Character[] team)
        {
            return team.Where(x => x.Hp != 0).FirstOrDefault();
        }

        public string Attack(Character attacker, Character victim)
        {
            if (victim.Hp <= attacker.Damage)
            {
                victim.Hp = 0;
                return $"{attacker.Name} kills {victim.Name}!";
            }

            victim.Hp = victim.Hp - attacker.Damage;
            return $"{attacker.Name} attacks {victim.Name} for {attacker.Damage}!";
        }

        public int GetFasterTeam(Character[] team1, Character[] team2)
        {
            var team1Speed = team1.Select(x => x.Speed).Sum();
            var team2Speed = team2.Select(x => x.Speed).Sum();

            return team1Speed >= team2Speed ? 1 : 2;
        }

        public bool ShouldCastSpell(int step, Character character)
        {
            return step % character.Cooldown == 0;
        }
    }
}