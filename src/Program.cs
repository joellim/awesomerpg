﻿using System;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using HumbleProgrammer.Characters;

namespace HumbleProgrammer
{
    [ExcludeFromCodeCoverage]
    class Program
    {
        public static void Main(string[] args)
        {
            var hj1 = new HawJia(0, "hj1");
            var hj2 = new HawJia(0, "hj2");
            var hj3 = new HawJia(0, "hj3");
            var hj4 = new HawJia(0, "hj4");
            var hj5 = new HawJia(0, "hj5");
            var hj6 = new HawJia(0, "hj6");
            var joe1 = new CustomChar(0, "joe1");
            
            var player1 = new Player(Guid.NewGuid(), new Character[] {hj1, hj2, hj3});
            var player2 = new Player(Guid.NewGuid(), new Character[] {hj4, hj5, hj6, joe1});

            var game = new Game(player1, player2);
            string[] logs;
            Player winner;
            (logs, winner) = game.SimulateMatch();
            
            logs.ToList().ForEach(Console.WriteLine);
            if (winner == player1)
            {
                Console.WriteLine("Player 1 wins hooray!");
            }
            else
            {
                Console.WriteLine("Player 2 wins hooray!");
            }
        }
    }
}