using System.Linq;
using FluentAssertions;
using HumbleProgrammer.Characters;
using Xunit;

namespace Tests
{
  
    public class HawJiaTest
    {
        [Fact]
        public void NameAssertion()
        {
            var hj = new HawJia(0);
            hj.Name.Should().Be("Haw Jia");
        }

        [Theory]
        [InlineData(0)]
        [InlineData(25)]
        public void LevelAssertion(int numWins)
        {
            var levelCalculator = HawJia.LevelCalculator;
            var hj = new HawJia(numWins);

            hj.Level.Should().Be(levelCalculator(numWins));
        }

        [Theory]
        [InlineData(0)]
        [InlineData(25)]
        public void CooldownAssertion(int numWins)
        {
            var cooldownCalculator = HawJia.CooldownCalculator;
            var hj = new HawJia(numWins);
            var level = hj.Level;

            hj.Cooldown.Should().Be(cooldownCalculator(level));
        }

        [Theory]
        [InlineData(0)]
        [InlineData(25)]
        public void DamageAssertion(int numWins)
        {
            var damageCalculator = HawJia.DamageCalculator;
            var hj = new HawJia(numWins);
            var level = hj.Level;

            hj.Damage.Should().Be(damageCalculator(level));
        }

        [Theory]
        [InlineData(0)]
        [InlineData(25)]
        public void SpeedAssertion(int numWins)
        {
            var speedCalculator = HawJia.SpeedCalculator;
            var hj = new HawJia(numWins);
            var level = hj.Level;

            hj.Speed.Should().Be(speedCalculator(level));
        }

        [Fact]
        public void SpecialActionAssertions()
        {
            var hj = new HawJia(20);

            var one_team_a = new MockCharacter("a1");
            var two_team_a = new MockCharacter("a2");
            var three_team_a = new MockCharacter("a3");

            var one_team_b = new MockCharacter("b1");
            var two_team_b = new MockCharacter("b2");
            var three_team_b = new MockCharacter("b3");

            Character[] allies = {one_team_a, two_team_a, three_team_a};
            Character[] enemies = {one_team_b, two_team_b, three_team_b};

            hj.SpecialAction(allies, enemies);

            allies.Should().ContainSingle(x => x.Name == "b1" || x.Name == "b2" || x.Name == "b3");
            enemies.Should().ContainSingle(x => x.Name == "a1" || x.Name == "a2" || x.Name == "a3");
        }
    }
}
