using FluentAssertions;
using HumbleProgrammer.Characters;
using Xunit;
using System;

namespace Tests {
    public class CustomCharTest {
        [Theory]
        [InlineData(1, "Dummy")]
        public void CooldownAssertions(int Level, string Name) {
            CustomChar customChar = new CustomChar(Level, Name);
            customChar.Cooldown.Should().Be(Math.Min(2, 5 - Level));
        }

        [Theory]
        [InlineData(1, "Dummy")]
        public void DamageAssertions(int Level, string Name) {
            CustomChar customChar = new CustomChar(Level, Name);
            customChar.Damage.Should().Be(1 + Level);
        }

        [Theory]
        [InlineData(1, "Dummy")]
        public void SpeedAssertions(int Level, string Name) {
            CustomChar customChar = new CustomChar(Level, Name);
            customChar.Damage.Should().Be(1 + Level);
        }

        [Fact]
        public void SpecialActionAssertions() {
            CustomChar customChar = new CustomChar(0, "1");
            var one_team_a = new MockCharacter("a1");
            var two_team_a = new MockCharacter("a2");
            var three_team_a = new MockCharacter("a3");

            var one_team_b = new MockCharacter("b1");
            var two_team_b = new MockCharacter("b2");
            var three_team_b = new MockCharacter("b3");

            Character[] allies = {one_team_a, two_team_a, three_team_a};
            Character[] enemies = {one_team_b, two_team_b, three_team_b};

            customChar.SpecialAction(allies, enemies);
            var Hp = -1;
            foreach (var enemy in enemies) {
                enemy.Hp.Should().Be(Hp);
                Hp -= 1;
            }
        }
    }
}