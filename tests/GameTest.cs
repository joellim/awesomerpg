using System.Collections.Generic;
using System.Linq;
using FluentAssertions;
using HumbleProgrammer;
using HumbleProgrammer.Characters;
using Xunit;

namespace Tests
{
    public class GameTest
    {
        [Fact]
        public void StepAssertion()
        {
            
        }

        [Theory]
        [InlineData(0, true)]
        [InlineData(3, true)]
        [InlineData(5, false)]
        [InlineData(10, false)]
        public void ShouldCastSpell_Cooldown3_Assertion(int step, bool shouldCast)
        {
            Character character = new MockCharacter(3);
            Game game = new Game(null ,null);

            game.ShouldCastSpell(step, character).Should().Be(shouldCast);
        }
        
        [Theory]
        [InlineData(0, true)]
        [InlineData(3, false)]
        [InlineData(5, true)]
        [InlineData(10, true)]
        public void ShouldCastSpell_CoolDown5_Assertion(int step, bool shouldCast)
        {
            Character character = new MockCharacter(5);
            var game = new Game(null ,null);

            game.ShouldCastSpell(step, character).Should().Be(shouldCast);
        }

        [Fact]
        public void GetFasterTeam_FirstTeamFaster_Assertion()
        {
            var game = new Game(null ,null);
            
            Character[] team1 = {new MockCharacter(2), new MockCharacter(2), new MockCharacter(1)};
            Character[] team2 = {new MockCharacter(1), new MockCharacter(1), new MockCharacter(1)};

            game.GetFasterTeam(team1, team2).Should().Be(1);
        }
        
        [Fact]
        public void GetFasterTeam_SecondTeamFaster_Assertion()
        {
            var game = new Game(null ,null);
            
            Character[] team1 = {new MockCharacter(1), new MockCharacter(1), new MockCharacter(1)};
            Character[] team2 = {new MockCharacter(2), new MockCharacter(2), new MockCharacter(1)};

            game.GetFasterTeam(team1, team2).Should().Be(2);
        }
        
        [Fact]
        public void GetFasterTeam_SameSpeed_Assertion1()
        {
            var game = new Game(null ,null);
            
            Character[] team1 = {new MockCharacter(1), new MockCharacter(1), new MockCharacter(1)};
            Character[] team2 = {new MockCharacter(1), new MockCharacter(1), new MockCharacter(1)};

            game.GetFasterTeam(team1, team2).Should().Be(1);
        }

        [Fact]
        public void Attack_NoDie_Assertion()
        {
            var game = new Game(null ,null);

            Character c1 = new MockCharacter(100);
            Character c2 = new MockCharacter(10);

            string log = game.Attack(c2, c1);
            c1.Hp.Should().Be(90);

            log.Should().Be($"{c2.Name} attacks {c1.Name} for {c2.Damage}!");
        }
        
        [Fact]
        public void Attack_Die_Assertion()
        {
            var game = new Game(null ,null);

            Character c1 = new MockCharacter(5);
            Character c2 = new MockCharacter(10);

            string log = game.Attack(c2, c1);
            c1.Hp.Should().Be(0);

            log.Should().Be($"{c2.Name} kills {c1.Name}!");
        }

        private (Character[], Character[]) generateTeams()
        {
            Character alex = new MockCharacter("Alex", 1);
            Character bruce = new MockCharacter("Bruce", 3);
            Character charlie = new MockCharacter("Charlie", 5);
            Character[] team1 = {alex, bruce, charlie};
            
            Character alan = new MockCharacter("Alan", 7);
            Character bob = new MockCharacter("Bob", 2);
            Character chan = new MockCharacter("Chan", 4);
            Character[] team2 = {alan, bob, chan};

            return (team1, team2);
        }

        [Fact]
        public void Step4_Assertion()
        {
            Game game = new Game(null, null);
            
            Character[] team1;
            Character[] team2;
            (team1, team2) = this.generateTeams();

            string[] expected = new[]
            {
                "Alan kills Alex!",
                "Bob performed special action!",
                "Bruce attacks Alan for 3!",
                "Chan performed special action!",
                "Charlie kills Alan!"
            };

            IEnumerable<string> logs = game.Step(4, team1, team2);

            logs.Should().Equal(expected);
            
            team1.Select(x => x.Hp).Should().Equal(new []{0, 3, 5});
            team2.Select(x => x.Hp).Should().Equal(new[] {0, 2, 4});
        }
        
        [Fact]
        public void Step12_Assertion()
        {
            Game game = new Game(null, null);
            
            Character[] team1;
            Character[] team2;
            (team1, team2) = this.generateTeams();

            string[] expected = new[]
            {
                "Alan kills Alex!",
                "Bob performed special action!",
                "Bruce performed special action!",
                "Chan performed special action!",
                "Charlie attacks Alan for 5!"
            };
            
            IEnumerable<string> logs = game.Step(12, team1, team2);

            logs.Should().Equal(expected);
            
            team1.Select(x => x.Hp).Should().Equal(new []{0, 3, 5});
            team2.Select(x => x.Hp).Should().Equal(new[] {2, 2, 4});
        }
    }
}