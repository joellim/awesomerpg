using HumbleProgrammer.Characters;

namespace Tests
{
    public class MockCharacter: Character
    {
        public string Name { get; } = "Dummy";
        public int Cooldown { get; } = 0;
        public int Damage { get; } = 0;
        public int Hp { get; set; } = 0;
        public int Speed { get; } = 0;

        public MockCharacter(string name)
        {
            Name = name;
        }

        public MockCharacter(int value)
        {
            Cooldown  = value;
            Damage = value;
            Hp = value;
            Speed = value;
        }

        public MockCharacter(string name, int value)
        {
            Name = name;
            Cooldown  = value;
            Damage = value;
            Hp = value;
            Speed = value;
        }
        
        public string[] SpecialAction(Character[] allies, Character[] enemies)
        {
            return this.GetSpecialActionLogs();
        }

        public string[] GetSpecialActionLogs()
        {
            return new[] {$"{Name} performed special action!"};
        }
    }
}